<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use HasFactory;
    public $first_last_name;
    public $second_last_name;
    public $first_name;
    public $second_name;
    public $country;
    public $id_type;
    public $number_id;
    public $email;
    public $date_adm;
    public $area;
    public $status;
    public $rdate;

    /**
     * Creates employee
     * @return void
     */
    public function createEmployee(){
        $responseValidation = $this->validateName();
        if ($responseValidation["result"] == "fail") {
            return [
                "result" => "fail",
                "response" =>$responseValidation["response"]    
            ];
        }

        $email = $this->generateEmail();
        DB::table("employees")->insert([
            'first_last_name' => $this->first_last_name,
            'second_last_name' => $this->second_last_name,
            'first_name' => $this->first_name,
            'second_name' => $this->second_name,
            'country' => $this->country,
            'id_type' => $this->id_type,
            'number_id' => $this->number_id,
            'email' => $email,
            'date_adm' => $this->date_adm,
            'area' => $this->area,
            'status' => 1,
            'rdate' => date("Y-m-d H:m:s"),
        ]);

        return [
            "result" => "ok"
        ];
    }

    private function generateEmail(){

        $domain = $this->country == "Colombia" ? "@cidenet.com.co" : "@cidenet.com.us";
        $email = strtolower($this->first_name . "." . $this->first_last_name);
        $r = DB::table("employees")->where('email', 'like', '%'.$email .'%')->where('country', '=', $this->country)->orderByRaw('id DESC')->limit(1)->get();

        if (count($r) > 0) {
            $emailRegistered = explode("@", $r[0]->email)[0];
            $emailRegistered = explode(".", $emailRegistered);
            if (count($emailRegistered)>2) {
                $emailID = intval($emailRegistered[2]) + 1;
                $email = $email . "." . $emailID . $domain;
            } else {
                $email = $email. ".1" . $domain;
            }
        } else {
            $email = strtolower($this->first_name . "." . trim(preg_replace(['/\s+/','/^\s|\s$/'],['',''], $this->first_last_name)) . $domain);
        }
        return $email;
    }

    private function validateName(){

        if (!preg_match('/^[a-zA-Z]*$/', $this->first_last_name)) {
            return [
                "result" => "fail",
                "response" => "Only characters A-Z and no ñ"
            ];
        }

        return [
            "result" => "ok"
        ];
    
    }

}
