<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("employees")->insert([
            'first_last_name' => 'Barrera',
            'second_last_name' => 'Mazo',
            'first_name' => 'Andres',
            'second_name' => 'Felipe',
            'country' => 'Colombia',
            'id_type' => 'Cedula de ciudadania',
            'number_id' => '1152716070',
            'email' => 'af-barrera@cidenet.edu.co',
            'date_adm' => '2022-06-23 22:38:00',
            'area' => 'Finaciera',
            'status' => 1,
            'rdate' => '2022-06-20 22:38:00'
        ]);
    }
}
