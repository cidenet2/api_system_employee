<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string("first_last_name");
            $table->string("second_last_name");
            $table->string("first_name");
            $table->string("second_name");
            $table->string("country");
            $table->string("id_type");
            $table->string("number_id");
            $table->string("email");
            $table->dateTime("date_adm");
            $table->string("area");
            $table->integer("status");
            $table->dateTime("rdate");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
